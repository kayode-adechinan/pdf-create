package com.hendisantika.pdfcreate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : pdf-create
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/05/20
 * Time: 12.48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Details {
    private String name;

    private int age;

    private String country;
}
